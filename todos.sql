CREATE TABLE tasks
(
  id INT AUTO_INCREMENT PRIMARY KEY,
  title NVARCHAR(200) NOT NULL,
  content NVARCHAR(9999) NOT NULL,
  cost INT DEFAULT 0,
  today BOOLEAN DEFAULT 1, /* 1 (TRUE) is default today, FALSE is todos */
  status BOOLEAN DEFAULT 0, /* 0 (FALSE) is open, TRUE is done */
  due_date INT NULL,
  due_month INT NULL,
  due_year INT NULL,
  create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP /* timezone UTC */
);

/* today, open, 0 cost */
INSERT INTO tasks (title, content, due_date, due_month, due_year) VALUES ('today 1','content today 1',11,11,2020);
INSERT INTO tasks (title, content, due_date, due_month, due_year) VALUES ('today 2','content today 2',11,11,2020);
INSERT INTO tasks (title, content, due_date, due_month, due_year) VALUES ('today 3','content today 3',11,11,2020);

/* today, done, 0 cost */
INSERT INTO tasks (title, content, status, due_date, due_month, due_year) VALUES ('today 4 done','content today 4 done',true, 11,11,2020);
INSERT INTO tasks (title, content, status,due_date, due_month, due_year) VALUES ('today 5 done','content today 5 done',true, 11,11,2020);
INSERT INTO tasks (title, content, status, due_date, due_month, due_year) VALUES ('today 6 done','content today 6 done',true, 11,11,2020);

/* today, open, cost */
INSERT INTO tasks (title, content, cost, due_date, due_month, due_year) VALUES ('today 1','content today 1',100000,11,11,2020);
INSERT INTO tasks (title, content, cost, due_date, due_month, due_year) VALUES ('today 2','content today 2',20000,11,11,2020);
INSERT INTO tasks (title, content, cost, due_date, due_month, due_year) VALUES ('today 3','content today 3',300000,11,11,2020);

/* today, done, cost */
INSERT INTO tasks (title, content, cost, status, due_date, due_month, due_year) VALUES ('today 4 done','content today 4 done',40000,true, 11,11,2020);
INSERT INTO tasks (title, content, cost, status,due_date, due_month, due_year) VALUES ('today 5 done','content today 5 done',50000,true, 11,11,2020);
INSERT INTO tasks (title, content, cost, status, due_date, due_month, due_year) VALUES ('today 6 done','content today 6 done',60000,true, 11,11,2020);

/* todos, open, 0 cost */
INSERT INTO tasks (title, content, today) VALUES ('todos 1','content todos 1',false);
INSERT INTO tasks (title, content, today) VALUES ('todos 2','content todos 2',false);
INSERT INTO tasks (title, content, today) VALUES ('todos 3','content todos 3',false);

/* todos, done, 0 cost */
INSERT INTO tasks (title, content, today, status) VALUES ('today 4 done','content today 4 done',false,true);
INSERT INTO tasks (title, content, today, status) VALUES ('today 5 done','content today 5 done',false,true);
INSERT INTO tasks (title, content, today, status) VALUES ('today 6 done','content today 6 done',false,true);

/* todos, open, cost */
INSERT INTO tasks (title, content, today, cost) VALUES ('today 1','content today 1',false, 100000);
INSERT INTO tasks (title, content, today, cost) VALUES ('today 2','content today 2',false, 20000);
INSERT INTO tasks (title, content, today, cost) VALUES ('today 3','content today 3',false, 300000);

/* todos, done, cost */
INSERT INTO tasks (title, content, cost, today, status) VALUES ('today 4 done','content today 4 done',40000,false,true);
INSERT INTO tasks (title, content, cost, today, status) VALUES ('today 5 done','content today 5 done',50000,false,true);
INSERT INTO tasks (title, content, cost, today, status) VALUES ('today 6 done','content today 6 done',60000,false,true);

/** API Query data **/
/* GET: tasks */
	/* Get list of tasks, filter by type */

	/* List today */
	SELECT * FROM tasks WHERE today = TRUE;
	/* List todos */
	SELECT * FROM tasks WHERE today = FALSE;
    
/* POST: tasks */
	/* Create a list new task */
    
/* GET: /tasks/sums */
	/* Count tasks */
    
    /* Count today task open */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = TRUE AND status = FALSE;
    /* Count today task total */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = TRUE;
    /* Count todos task open */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = FALSE AND status = FALSE;
    /* Count todos task total */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = FALSE;
    
/* GET: /tasks/histories */
	/* Get list of task completed */
    
    /* Count today task done */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = TRUE AND status = TRUE;
    /* Count today task total */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = TRUE;
    /* Count todos task open */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = FALSE AND status = TRUE;
    /* Count todos task total */
    SELECT COUNT(id)
    FROM tasks
    WHERE today = FALSE;
    
/* GET: /tasks/spends */
    /* Get receipt by year, list month, money... */
    
    /* Data of today task in year */
    SELECT due_year AS year, SUM(cost) AS total
    FROM tasks
    WHERE today = TRUE
    GROUP BY due_year;
    
    /* Data of today task in month */
    SELECT due_month AS month, SUM(cost) AS total
    FROM tasks
    WHERE today = TRUE AND due_year = 2020
    GROUP BY due_month;
    
    /* Data of todos task in year */
    SELECT due_year AS year, SUM(cost) AS total
    FROM tasks
    WHERE today = FALSE
    GROUP BY due_year;
    
    /* Data of todos task in month */
    SELECT due_month AS month, SUM(cost) AS total
    FROM tasks
    WHERE today = FALSE AND due_year = 2020
    GROUP BY due_month;