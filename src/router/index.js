import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: () => import("@/views/Dashboard")
  },
  {
    path: "/new",
    name: "AddNew",
    component: () => import("@/views/AddNew")
  },
  {
    path: "/spend",
    name: "Spend",
    component: () => import("@/views/Spend")
  },
  {
    path: "/history",
    name: "History",
    component: () => import("@/views/History")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
